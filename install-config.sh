#!/bin/bash

# Install files
cp -i ./.zshrc ~/.zshrc
cp -i ./.zsh_alias ~/.zsh_alias

# Make supplemental files
touch ~/.zsh_history
touch ~/.zsh_private
