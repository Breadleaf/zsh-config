#!/bin/bash

# Update configs
cp ~/.zshrc .
cp ~/.zsh_alias .

# Get update message
read -p "> " update_name

# Update repo
git add --all
git commit -m "Repo Update: ${update_name}"
git push
