####################################################

# By: Bradley Hutchings
# Desc: This is my .zshrc file. (ZSH Shell Config)

####################################################

# https://gitlab.com/breadleaf

####################################################

# Settings - Bindings - Exports

####################################################

setopt PROMPT_SUBST # Allow for substrings in PS1
setopt aliases

unset EXA_ICON_SPACING
export EXA_ICON_SPACING=2

# zsh history file
export HISTFILE=~/.zsh_history
export HISTSIZE=10000
export SAVEHIST=10000
setopt appendhistory

####################################################

# Aliases

####################################################

unalias -a

alias srcz="source ~/.zshrc"
alias edz="vim ~/.zshrc"
alias edzh="vim ~/.zsh_history"

source ~/.zsh_private
source ~/.zsh_alias

####################################################

# Prompt

####################################################

PROMPT=$'\n'"┌{%~}-{"'$(git branch 2>/dev/null | grep \* | colrm 1 2'")}"$'\n'"└>> "
